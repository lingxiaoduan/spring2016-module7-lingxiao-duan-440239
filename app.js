'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
  'ngRoute',
  'phonecatControllers'
]);

phonecatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/phones', {
        templateUrl: 'partials/phone-list.html',
        controller: 'PhoneListCtrl'
      }).
      when('/phones/1', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneReview1'
      }).
	        when('/phones/2', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneReview2'
      }).
	        when('/phones/3', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneReview3'
      }).
	        when('/phones/4', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneReview4'
      }).
      otherwise({
        redirectTo: '/phones'
      });
  }]);
